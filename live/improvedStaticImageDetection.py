'''
Author: Thong Vu
SID: 011477069
Email: thong.vu@wsu.edu
'''

import cv2 as cv
import numpy as np
import sys
import threading
import xml.etree.ElementTree as tree
import xml.dom.minidom as dom
import argparse
import random
import os
import time

growBorderValue = 15 # Amount of pixel padding for extraction
data = tree.Element("data") #XML data
image_data = None # XML element that holds information about the image used for extraction
testing_image_data = None # XML element that holds information about the image used for extraction for testing set
testing_lego_list = None #  XML element that holds information about the legos found in the image for testing set
verbose = False # Flag for console output
keepSize = False # Flag for downscaling images
writeLog = True # Flag for writing output
performRecalibration = True #Flag for performing image pre-process calibration
saveThreshold = False #Flag to save detector
compressSize = 64 # value to set size of downscaled image
createArtificialData = False # Flag to create artificial data
useThread = False #Flag used for multithreading 
lockXmlWrite = threading.Lock() # Lock for critical sectors
lockCounter = threading.Lock() # Lock for critical sectors
getTestingSet = False
nSets = 3 # number of training/testing sets

backgroundColor = [145,85,210]
backgroundLABColor = [140,180,120]
hasSetBackgroundValue = False

completedImages = [] #list for extracted images
processedImages = 0 #count how many images in folder are processed
numberOfExtractedImages = 0
numberOfImagesInFolder = 0

def nothing(x):
	pass
	
def setBackgroundValues(image):
	"""
		Gather average RGB and L*a*b* values of an image for background subtraction
	"""
	channel = cv.split(image)
	global backgroundColor
	backgroundColor[0] = np.average(channel[0])
	backgroundColor[1] = np.average(channel[1])
	backgroundColor[2] = np.average(channel[2])
	backgroundColor = np.uint8(backgroundColor)
	
	lab = cv.cvtColor(image,cv.COLOR_BGR2LAB)
	lab_channel = cv.split(lab)
	global backgroundLABColor
	backgroundLABColor[0] = np.average(lab_channel[0])
	backgroundLABColor[1] = np.average(lab_channel[1])
	backgroundLABColor[2] = np.average(lab_channel[2])
	backgroundLABColor = np.uint8(backgroundLABColor)
	hasSetBackgroundValue = True
	p = print(f'Calibration done. Background color (BGR):{backgroundColor}') if verbose else None

def imageCalibration(image, fromFrame=False):
	'''
		shift the image's color-space to the correct values
		
		background values presumed to be a shade of bright-pink of RGB values (145,85,210)
	'''

	p = print("Image correcting...",end='') if verbose else None
	
	r_avg = np.average(cv.split(image)[2])
	g_avg = np.average(cv.split(image)[1])
	b_avg = np.average(cv.split(image)[0])
	r_base = backgroundColor[2]
	g_base = backgroundColor[1]
	b_base = backgroundColor[0]
	if abs(r_avg-r_base) > 20 or abs(g_avg-g_base) > 20 or abs(b_avg-b_base) > 20:	
		lab = cv.cvtColor(image,cv.COLOR_BGR2LAB)
		lab_channel = cv.split(lab)

		l_avg = np.mean(lab_channel[0])
		a_avg = np.mean(lab_channel[1])
		b_avg = np.mean(lab_channel[2])
		l_base = backgroundLABColor[0]
		a_base = backgroundLABColor[1]
		b_base = backgroundLABColor[2]
		if abs(a_base-a_avg) > 10 or abs(b_base-b_avg) > 10:
			diff = np.subtract([l_base,a_base,b_base],[l_base,a_avg,b_avg])
			
			new = np.add(lab,diff)
			new = np.uint8(new)
			new = cv.cvtColor(new,cv.COLOR_LAB2BGR)
			p = print(f"Done\nShift L*a*b* color space by {np.uint8(diff)}") if verbose else None
			return new
		p = print("No Correction needed") if verbose else None
	return image
	
def detectBlocks(image, image_name="", readjusting=False,fromFrame=False,closingSize=15):
	"""
		Takes an image and returns a bi-color (black and white) image
		that shows the area of which it is (hopefully) not the background
		
		Parameters:
		image: A 3 channel image
		image_name: Used for naming the saved returned image (Used when global value 'saveThreshold' is true)
		readjusting: Used when the extracted Legos goes through post-processing
		fromFrame: Used to detect Legos during live video capture
		closingSize: Used in noise reduction
		Returns a bi-color (black and white) image
	"""
	original = image.copy()
	if not readjusting:
		global backgroundColor
		channel = cv.split(original)
		backgroundColor[0] = np.uint8(np.average(channel[0][:128,:128]))
		backgroundColor[1] = np.uint8(np.average(channel[1][:128,:128]))
		backgroundColor[2] = np.uint8(np.average(channel[2][:128,:128]))
	
	hsv = cv.cvtColor(image,cv.COLOR_BGR2HSV) #convert to hsv
	channel = cv.split(hsv)
	
	# set range of tolerance
	hue_tol = [0.92,1.05]
	sat_tol = [0.5,1]
	if not readjusting:
		roi_height, roi_width = 128,128
	else:
		roi_height, roi_width = max(1,round(image.shape[0]*0.05)),max(1,round(image.shape[1]*0.05))
	#gather average values at top-left corner
	avg_hue = np.uint8(np.average(channel[0][:roi_height,:roi_width]))
	avg_sat = np.uint8(np.average(channel[1][:roi_height,:roi_width]))
	avg_val = np.uint8(np.average(channel[2][:roi_height,:roi_width]))
	lower = np.array([avg_hue*hue_tol[0],150,150])
	upper = np.array([avg_hue*hue_tol[1],255,255])
	hsv = cv.inRange(hsv,lower,upper)
	hsv = cv.morphologyEx(hsv,cv.MORPH_CLOSE,np.ones((closingSize,closingSize),np.uint8)) # Noise reduction
	# hsv = cv.morphologyEx(hsv,cv.MORPH_OPEN,np.ones((closingSize*2,closingSize*2),np.uint8)) # Gap fill
	hsv = cv.bitwise_not(hsv) #invert image for contours
	
	#output log
	if not readjusting or not fromFrame:
		p = print("Avg. (Hue,Sat,Val) at (0,0) with shape ("+str(roi_width)+"x"+str(roi_height)+f"): ({avg_hue/255*360,avg_sat/255*100,avg_val/255*100}).") if verbose else None
		if writeLog and not readjusting:
			global image_data
			image_data = tree.SubElement(data,"image",name=image_name)
			tree.SubElement(image_data,"Width").text = str(original.shape[1])
			tree.SubElement(image_data,"Height").text = str(original.shape[0])
			tree.SubElement(image_data,"AvgHue").text = str(avg_hue)
			tree.SubElement(image_data,"AvgSat").text = str(avg_sat)
			tree.SubElement(image_data,"AvgVal").text = str(avg_val)
			global lego_list
			lego_list = tree.SubElement(image_data,"legos")
			# print(image_name)
			
	if saveThreshold and len(image_name) > 0:
		cv.imwrite('./'+image_name+"_detector.png",hsv)
	return hsv #now a bi-color (b and w) image


def extractBlocks(original, detector, useHull=False,fromFrame=False):
	"""
		Takes image detector and find region of interest. Then crop and process the original image by
		making a copy of it based on the region of interest
		
		Parameters:
		original: the original 3 channel image
		detector: detector image returned by detectBlocks
		useHull: When true, method will use best fit triangle. Otherwise, angled rectangle is used for extraction
		fromFrame: Used to detect Legos during live video capture
		
		useHull: use straight triangles (True) instead of rotated triangle (False), false by default
		yield tuples (id, image, color)
	"""
	assert(original.shape[0] == detector.shape[0] and original.shape[1] == detector.shape[1])
	# Rotated rectangle
	im2, contours, hierarchy = cv.findContours(detector,cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE) #find countours
	p = print("Finding Legos...",end="") if verbose else None
	p = print("Found "+str(len(contours))+" potential Legos") if verbose else None
	if writeLog:
		block_data = tree.SubElement(lego_list,"ShapesFound").text = str(len(contours))
	extracted = 0
	for c in contours:
		# IMAGE SEGMENTING
		if useHull:
			#best fit rectangle
			x,y,w,h = cv.boundingRect(cv.convexHull(c,False))
		else:
			#angled rectangle
			rect = cv.minAreaRect(c)
			box = cv.boxPoints(rect)
			box = np.int0(box)

			x, y, w, h = cv.boundingRect(box)
		#grow border by 10 pixels
		x -= growBorderValue
		y -= growBorderValue
		w += growBorderValue*2
		h += growBorderValue*2
		
		cropped = original[max(0,y):min(original.shape[0],y+h),max(0,x):min(original.shape[1],x+w)] #grab lego
		h, w = cropped.shape[0], cropped.shape[1] #update height and width value for segmented imae
		cornerSection = cv.split(cropped[:32,:32]) #get the bgr channel for a corner 
		if len(cornerSection) == 0 or (w-(growBorderValue*2) < 32 and h-(growBorderValue*2) < 32):
			# discard if its an invalid shape (i.e. too small for a 1x1 lego)
			p = print(f"A shape has been discarded at position ({x},{y}) with shape {w}x{h}") if verbose else None
			continue
		global numberOfExtractedImages
		for angle in range(0,360 if createArtificialData else 1,60):
			extracted += 1
			if writeLog or not fromFrame:
				block_data = tree.SubElement(lego_list,"lego_data",name="("+str(extracted)+")")
				tree.SubElement(block_data,"RawLocationX").text = str(x+growBorderValue)
				tree.SubElement(block_data,"RawLocationY").text = str(y+growBorderValue)
				tree.SubElement(block_data,"Width").text = str(w-growBorderValue*2)
				tree.SubElement(block_data,"Height").text = str(h-growBorderValue*2)
				tree.SubElement(block_data,"BlockType").text = "TYPE"
				tree.SubElement(block_data,"BlockDimension").text = "(W,H,D)"
				tree.SubElement(block_data,"BlockColor").text = "(B,G,R)"
				tree.SubElement(block_data,"BlockOrientation").text = str(("TOP","BOTTOM","SIDE"))
			p = print(f"ID {numberOfExtractedImages}: size {w}x{h} at position ({x},{y}) on raw.") if verbose else None
			color = [np.average(cornerSection[0]),np.average(cornerSection[1]),np.average(cornerSection[2])]
			yield [extracted, cropped, color,angle, (x,y,w,h)]
		numberOfExtractedImages += 1
		# END OF SEGMENTING

def processImages(images):
	"""
		Input takes a generator of images generated by extractBlocks and fill the noised region with
		a single color
		
		yields all processed image
	"""
	p = print("Processing Image...") if verbose else None
	for i, e, color, angle, location in images:
		color = [145,85,210]#backgroundColor#[145,85,210]
		#resizing images to a square
		newSize = (max(e.shape[0],e.shape[1]),max(e.shape[0],e.shape[1]),3) #segmented image size
		bordered = np.full(newSize,color,np.uint8)#fill new image with color
		
		min_x, min_y = (newSize[1]-e.shape[1])//2,(newSize[0]-e.shape[0])//2 #min x,y starting location
		bordered[min_y:min_y+e.shape[0],min_x:min_x+e.shape[1]] = e #paste cropped image in to new image
		# image denoising
		#bordered = cv.fastNlMeansDenoisingColored(bordered,None,10,10,21,7)
		
		# refill noised region with 1 color
		e_thresh = detectBlocks(bordered,readjusting=True) #get threshold
		r, c = np.where(e_thresh == 0) #find where threshold is black
		# new = e #grab the newly cropped image
		p = print("Fill non-Lego region with color "+str(np.uint8(color))) if verbose else None
		bordered[r,c] = color # fill interested region with color
		
		bordered = cv.resize(bordered,(compressSize,compressSize)) if not keepSize else bordered #Resizing for DB
		if angle > 0:
			#make artificial data
			matrix = cv.getRotationMatrix2D((bordered.shape[1]/2,bordered.shape[0]/2),angle,1)
			bordered = cv.warpAffine(bordered,matrix,(bordered.shape[1],bordered.shape[0]),flags=cv.INTER_NEAREST)
			
			mask = cv.bitwise_not(cv.inRange(bordered,(0,0,0),(0,0,0)))
			y, x = np.where(mask == 0) #find where threshold is black
			
			bordered[y,x] = color # fill interested region with color
		yield [i, bordered, location]
		
def gatherImagesFromFrame(image):
	detector = detectBlocks(image,fromFrame=True)
	extracted = extractBlocks(image,detector,fromFrame=True)
	extractedImages = processImages(extracted)
	completedImages.clear()
	return extractedImages
	
def gatherImagesFromFile(image_name, image_ext):
	image_path = image_name+image_ext
	p = print('-'*30) if verbose else None
	p = print("Opening "+image_name+"...",end="") if verbose else None
	if os.path.exists(image_path):
		try:
			image = cv.imread(image_path)
			if not type(image) == np.ndarray:
				raise
			p = print("OK") if verbose else None
		except:
			print("Failed") if verbose else None
			return
			
	if not os.path.isdir("./database"):
		os.mkdir("./database")
	#calibration
	if performRecalibration:
		image = imageCalibration(image)
	
	#perform extractions
	if useThread:
		#using threading
		lockXmlWrite.acquire() if writeLog else None
		detector = detectBlocks(image,image_name=image_name)
		extracted = extractBlocks(image,detector)
		extractedImages = processImages(extracted)
		#saving cropped images
		for id, image, location in extractedImages:
			completedImages.append([id,image,image_name])
		
		lockXmlWrite.release() if writeLog else None
		
		global processedImages
		processedImages += 1
		statusString = f"Processed {processedImages}/{numberOfImagesInFolder} images"
		print(statusString,end='\r' if not verbose or processImages == numberOfImagesInFolder else '\n')
	else:
		#using pipe-line
		detector = detectBlocks(image,image_name=image_name)
		extracted = extractBlocks(image,detector)
		extractedImages = processImages(extracted)
		#saving cropped images
		for id,image,location in extractedImages:
			completedImages.append([id,image,image_name])
	
def gatherImagesFromFolder(folderName):
	print("Checking if '"+folderName+"' folder exists...",end="")
	if not os.path.isdir(folderName):
		print("Folder '"+folderName+"' does not exist")
	else:
		print('OK')
		global numberOfImagesInFolder
		numberOfImagesInFolder = len([f for f in os.listdir(folderName) if not os.path.isdir(folderName+"/"+f)])
		global processedImages
		processedImages = 0
		statusString = ""
		fileList = os.listdir(folderName)
		if useThread:		
			threadList = []
		i = 0
		statusString = f"Processed 0/{numberOfImagesInFolder} images"
		print(statusString,end='\r' if not verbose or processImages == numberOfImagesInFolder else '\n')
		for f in fileList:
			if not os.path.isdir(folderName+"/"+f):
				f = folderName+"/"+f
				dotLoc = f.rfind(".")
				name, ext = (f[:dotLoc],f[dotLoc:])
				startTime = time.time()
				if useThread:
					threadList.append(threading.Thread(target=gatherImagesFromFile,args=(name,ext)))
				else:
					gatherImagesFromFile(name,ext)
					processedImages += 1
					statusString = f"Processed {processedImages}/{numberOfImagesInFolder} images"
					print(statusString,end='\r' if not verbose or processImages == numberOfImagesInFolder else '\n')
		if useThread:
			for i in range(len(threadList)): #while threading.active_count() < 4 and i < len(threadList):
				try:
					threadList[i].start()
					i += 1
				except RuntimeError:
					print('Cannot start thread %i', i)
			for i in range(len(threadList)):
				try:
					threadList[i].join()
				except RuntimeError:
					print('Cannot join thread!')
		writeImages(clearList=True)

def cleanDatabase():
	# Bad code smell
	print("Cleaning database...")
	print('Checking if database folder exists...',end="")
	if not os.path.isdir("./database/"):
		print("FAILED")
		print("Folder does not exist")
	else:
		print('OK')
		os.chdir(os.getcwd()+"/database/")
		for f in os.listdir("."):
			if not os.path.isdir(f):
				print("Removed "+f)
				os.remove(f)
		os.chdir("..")

def writeImages(clearList=False):
	print("Outputting Images..."+' '*30,end='\r')
	if getTestingSet:
		global completedImages
		shuffledCompletedImages = random.sample(completedImages,len(completedImages)) #shuffle images
		extractRange = [x for x in range(-1,len(shuffledCompletedImages),len(shuffledCompletedImages)//max(nSets,2))]
		
		import copy
		for r in range(len(extractRange)-1):
			low = extractRange[r]+1
			high = extractRange[r+1]
			
			testingDir = './database/testing'+str(r)+'/'
			testingLog = tree.Element('testData')
			os.mkdir(testingDir) if not os.path.isdir(testingDir) else None
			#output testing set
			for id, image, image_name in shuffledCompletedImages[low:high]:
				#reads built xml log
				for i in data:
					if not i.get('name') == None:
						if i.get('name') == image_name:
							for l in i.find('legos') if not i.find('legos') == None else []:
								if not l.get('name') == None:
									name = l.get('name')
									if name[name.find('(')+1:name.rfind(')')] == str(id):
										e_copy = copy.deepcopy(l)
										e_copy.set('name',testingDir+image_name[image_name.rfind('/')+1:]+name)
										tree.ElementTree(testingLog).getroot().append(e_copy)
				if cv.imwrite(testingDir+image_name[image_name.rfind("/")+1:]+"("+str(id)+").png",image):
					p = print("Image output successful at location '"+os.getcwd()+"/database/testing"+str(r)+"/"+image_name+"("+str(id)+").png'") if verbose else None
				else:
					p = print("Image output failed '") if verbose else None
			print(f'Outputing testing set {r}'+' '*30,end='\r')
			xml = tree.tostring(tree.ElementTree(testingLog).getroot())
			pretty_xml = dom.parseString(xml).toprettyxml()
			file = open(testingDir+"log.xml",'w')
			file.write(pretty_xml)
			file.close()
		for r in range(len(extractRange)-1):
			low = extractRange[r]+1
			high = extractRange[r+1]
			trainingDir = './database/training'+str(r)+'/'
			trainingLog = tree.Element('trainData')
			os.mkdir(trainingDir) if not os.path.isdir(trainingDir) else None
			#output training set
			for id, image, image_name in shuffledCompletedImages[0:low] + shuffledCompletedImages[high:]:
				#reads built xml log
				for i in data:
					if not i.get('name') == None:
						if i.get('name') == image_name:
							for l in i.find('legos') if not i.find('legos') == None else []:
								if not l.get('name') == None:
									name = l.get('name')
									if name[name.find('(')+1:name.rfind(')')] == str(id):
										e_copy = copy.deepcopy(l)
										e_copy.set('name',trainingDir+image_name[image_name.rfind('/')+1:]+name)
										tree.ElementTree(trainingLog).getroot().append(e_copy)
				if cv.imwrite(trainingDir+image_name[image_name.rfind("/")+1:]+"("+str(id)+").png",image):
					p = print("Image output successful at location '"+os.getcwd()+"/database/training"+str(r)+"/"+image_name+"("+str(id)+").png'") if verbose else None
				else:
					p = print("Image output failed '") if verbose else None
			print(f'Outputing training set {r}'+' '*30,end='\r')
			xml = tree.tostring(tree.ElementTree(trainingLog).getroot())
			pretty_xml = dom.parseString(xml).toprettyxml()
			file = open(trainingDir+"log.xml",'w')
			file.write(pretty_xml)
			file.close()
	else:
		count = 0
		for id, image, image_name in completedImages:
			if cv.imwrite("./database/"+image_name[image_name.rfind("/")+1:]+"("+str(id)+").png",image):
				p = print("Image output successful at location '"+os.getcwd()+"/database/"+image_name+"("+str(id)+").png'") if verbose else None
				count += 1
			else:
				p = print("Image output failed '") if verbose else None
			print(f'Wrote {count}/{len(completedImages)} images'+' '*30,end='\r')
	if clearList:
		completedImages.clear()
		
if __name__ == "__main__":
	
	parser = argparse.ArgumentParser(description="""Extract Legos from images, store extracted images
	in a local folder named 'database' and writes an XML log file""")
	parser.add_argument('--artificial_data',action='store_true',help="Create artificial data")
	parser.add_argument('--clean',action='store_true',help="Delete all files (not directories) of the database folder located in "+os.getcwd()+"/database/.")
	parser.add_argument('--compress_size',type=int,default=64,help="Change the size of the compressed image. Does nothing when --keep_size is present. Default value is 32")
	parser.add_argument('--folder',type=str,help="Folder of images used for extraction")
	parser.add_argument('--keep_size',action='store_true',help="Keep original crop size.")
	parser.add_argument('--no_log',action='store_false',help="Do not write log file")
	parser.add_argument('--thread',action='store_true',help="Do not use multi-threading")
	parser.add_argument('--no_recal',action='store_false',help="Use image as is")
	parser.add_argument('--image',type=str,help=f"""Image used for extraction. Determine where are Legos, extract and compress the images to 32x32
		and write each extracted images to {os.getcwd()}/database/.""")
	parser.add_argument('--save_detector',action='store_true',help=f"Store detector image where the input image is located.")
	parser.add_argument('--split_data',type=int,action='store',help="Separate some Legos in the set of images for testing in NN")
	parser.add_argument('--verbose',action='store_true',help="Output debugging info")
	if len(sys.argv) == 1:
		parser.print_help(sys.stdout)
		exit(0)

	args = parser.parse_args()
	if args.clean:
		cleanDatabase()
	args = parser.parse_args()
	verbose = args.verbose
	keepSize = args.keep_size
	compressSize = args.compress_size
	writeLog = args.no_log
	performRecalibration = args.no_recal
	createArtificialData = args.artificial_data
	saveThreshold = args.save_detector
	getTestingSet = False if args.split_data == None else True
	nSets = 1 if args.split_data == None else args.split_data
	useThread = args.thread
	startTime = time.time()

	p = print(f'''Create Artificial Data: {args.artificial_data}
Compress Size: {args.compress_size}x{args.compress_size}
Folder for extractions: {args.folder}
Image for extraction: {args.image}
Use Original Crop Size: {args.keep_size}
Log: {"Write" if args.no_log else "Do not Write"}
Multi-threading: {args.thread}
Recalibration: {args.no_recal}
Create Testing Set: {args.split_data}
Save image detectors: {args.save_detector}\n'''+'-'*30) if verbose else None

	if writeLog:
		tree.SubElement(data, "growBorderValue").text = str(growBorderValue)
		tree.SubElement(data, "keepSize").text = str(keepSize)
		tree.SubElement(data, "compressSize").text = str(compressSize)
		tree.SubElement(data, "splitData").text = str(getTestingSet)
		tree.SubElement(data, "numPartitions").text = str(nSets)

	if args.image:
		dotLoc = args.image.rfind(".")
		name, ext = (args.image[:dotLoc],args.image[dotLoc:])
		gatherImagesFromFile(name,ext)
		writeImages()
	elif args.folder:
		gatherImagesFromFolder(args.folder)
	
	
	if not args.no_log or not args.clean:
		#Creating XML tree
		xml = tree.tostring(tree.ElementTree(data).getroot())
		pretty_xml = dom.parseString(xml).toprettyxml()
		file = open("./database/log.xml",'w')
		file.write(pretty_xml)
		file.close()
		if not args.clean:
			totalTime = time.time()-startTime
			avgTime = totalTime/numberOfImagesInFolder if numberOfImagesInFolder > 0 else totalTime
			print("Time taken for operation: {:0.2f}s".format(totalTime))
			print("Avg. time per image : {:0.2f}s".format(avgTime))
			print(f"{numberOfExtractedImages} images written.")
			print('-'*30)
			