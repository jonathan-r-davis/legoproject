'''
Author: Jonathan Davis
Email: jonathanr.davis96@gmail.com
'''

import cv2 as cv
import numpy as np
import improvedStaticImageDetection as imageDetector
import training
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# cap = cv.VideoCapture(0)

url='http://192.168.43.1:8080/video'
cap = cv.VideoCapture(url)

# useCluster = False
canCapture = True
valueSet = False
# backSub = cv.createBackgroundSubtractorMOG2(detectShadows=False)
backSub = cv.createBackgroundSubtractorMOG2(history=30,detectShadows=False)

# print(cap.set(37,1))

while cap.isOpened():
	ret, frame = cap.read()
	if ret:
		cv.imshow('res', imageDetector.detectBlocks(frame,fromFrame=True))#,useKClust=useCluster))
		detector = backSub.apply(frame)
		detector = cv.morphologyEx(detector,cv.MORPH_OPEN,np.ones((5,5),np.uint8)) # Noise reduction
		x, y  = np.where(detector == 255)
		frame = imageDetector.imageCalibration(frame)
		if canCapture:
			if len(x)*len(y) < np.prod(detector.shape)*0.1:
				imageList = list(imageDetector.gatherImagesFromFrame(frame))
				print(f"Found {len(imageList)} shapes",end='\r')
				for id, piece, location in imageList:
					x, y ,w, h = location
					pieceChannel = cv.split(piece)
					predictions = training.model.predict(np.expand_dims(piece,0))
					pieceName = training.typeNameDict[np.argmax(predictions)]
					cv.putText(frame,pieceName,(x,y+h-4),cv.FONT_HERSHEY_SIMPLEX,1,(255,0,0),2,cv.LINE_AA)
					cv.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
				cv.putText(frame, str(len(imageList)),(32,96),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2,cv.LINE_AA)
	# cv.putText(frame,"K-Mean Detect." if useCluster else "",(32,64),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2,cv.LINE_AA)
	cv.imshow('live', frame)
	cv.imshow('detector',detector)
	# if cv.waitKey(1) == ord('c'):
	# 	useCluster = not useCluster
	if cv.waitKey(1) == ord('e'):
		canCapture = not canCapture
	elif cv.waitKey(1) == ord('s'):
		imageDetector.setBackgroundValues(frame)
		valueSet = not valueSet
	elif cv.waitKey(1) == ord('q'):
		break
	cv.putText(frame,"Rec" if canCapture else "",(32,32),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2,cv.LINE_AA)
	cv.putText(frame,"Values" if valueSet else "",(32,64),cv.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2,cv.LINE_AA)

cap.release()
cv.destroyAllWindows()