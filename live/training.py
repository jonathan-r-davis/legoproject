'''
Author: Jonathan Davis
Email: jonathanr.davis96@gmail.com
'''

import os
import cv2 as cv
import tensorflow as tf
import keras
from keras.models import load_model
from keras.utils import CustomObjectScope, np_utils
from keras.initializers import glorot_uniform
import numpy as np
import matplotlib.pyplot as plt

usePremadeModel = False
if os.path.isfile('./model_with_dropout_22_epochs'):
    print("Pre-made model found")
    usePremadeModel = True

typeNameDict = [
    'hook',
	'brick',
	'skinny',
	'slant',
	'catamaran',
	'corner',
	'legs'
]

if not usePremadeModel:
    train_images = []
    train_labels = []

    #Get training images & labels
    file = open('./database/log.csv','r')
    while True:
        line = file.readline().split(',')
        if len(line) == 1:
            break
        imageName = line[0]+".png"
        image = cv.imread(imageName) #color
        # image = cv.imread(imageName, 0) #b/w
        train_labels.append(line[1])
        train_images.append(image)
    train_labels = np.array(train_labels)
    train_images = np.array(train_images)
    file.close()


    # Process training & testing images and labels the same way
    train_images = train_images.astype('float32') / 255
    train_images = train_images.reshape(train_images.shape[0], 64, 64, 3) #color
    # train_images = train_images.reshape(train_images.shape[0], 64, 64, 1) #b/w
    train_labels = np_utils.to_categorical(train_labels, 7)


    #Build Model
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(32, (5,5), activation='relu', input_shape=(64,64,3)))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (5,5), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Conv2D(128, (5,5), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(1000, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.25))
    model.add(tf.keras.layers.Dense(7, activation='softmax'))

    #Compile model
    model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])

    #Fit model
    model.fit(train_images,train_labels,
            batch_size=50,epochs=22)

    model.save('./model_22_epochs_5_kernel')

else:
    with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
        model = load_model('./model_with_dropout_22_epochs')
