# Lego Identification Project:

Uses OpenCV to detect and process Legos from images/video frames and then uses TensorFlow model to predict what the parsed Lego image is.

#### /legoproject
    xmlConverter.py:
        -Turn .xml files in /database into .csv.
        -folder must be called /database

    findBestModel.py:
        -this is used to test different models over 3 sets of training/testing data in /database.

    compressedSets contains:
        -images (used to create full working model (/live/training.py), no test images in this set..)
        -database_3_sets_artificial (used to test models.. (findBestModel.py), this set contains artifical images made from /live/improvedStaticImageDetection.py)
        -database_3_sets (used to test models.. (findBestModel.py), no artifical images..)

#### /legoproject/live
    improvedStaticImageDetection.py:
        -OpenCV code that does all processing/creation of lego images from full capture

    liveDetection.py:
        -Used to show live video feed from phone camera. 
        -Connects to IPWebcam server and grabs video.
        -Uses improvedStaticImageDetection.py to detect and process legos in video frames
        -Uses training.py to load model
        -Uses split lego image piece & model to predict lego piece

    /database:
        -contains all images to create model, does not have testing set as the tests we will be doing on this model are the live demo pieces

#### Live demo instruction:
    -Install IPWebcam on phone, or use USB webcam.
        -In liveDetection.py, must update 'url' if using IPWebcam, or change 'cap' to use your webcam VideoCapture.
    -Ensure model in training.py is correct model in path.
    - $ python3 liveDetection.py

