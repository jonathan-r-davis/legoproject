from xml.etree import ElementTree
import os
import sys

#turns xml to csv

folderName = sys.argv[1]
tree = ElementTree.parse(folderName+'/log.xml')
root = tree.getroot()
typeNameDict = {
	'hook':0,
	'brick':1,
	'skinny':2,
	'slant':3,
	'catamaran':4,
	'corner':5,
	'legs':6,
	'TYPE':7
}
excludeTag = (
	'RawLocationX',
	'RawLocationY',
	'Width',
	'Height'
)
isSplit = root.find('splitData').text
if isSplit == 'True':
	for dir in os.listdir(folderName):
		if os.path.isdir(folderName+'/'+dir):
			file = open(folderName+'/'+dir+'/log.csv','w')
			tree = ElementTree.parse(folderName+'/'+dir+'/log.xml')
			root = tree.getroot()
			for l in root:
				fileName = l.get('name')
				if not fileName == None:
					newFileName = fileName.replace('./database',folderName)
					file.write(newFileName+',')
					for d in l:
						tag = d.tag
						if tag not in excludeTag:
							if tag == 'BlockType':
								#s = str(typeNameDict[d.text])
								f = fileName[fileName.find('/')+1:]
								s = f[f.find('_')+1:f.rfind('_')]
								s = s[:s.find('_') if not s.find('_') == -1 else len(s)]
								s = str(typeNameDict[s])
							elif tag == 'BlockDimension':
								f = fileName[fileName.rfind('/')+1:]
								s = f[:f.find('_')].replace('x',',')
								s = '('+s+')'
							else:
								s = d.text
							
							s = s+','
							file.write(s)
					file.write('\n')
			file.close()
elif isSplit == 'False':
	print('s')
	file = open(folderName+'/log.csv','w')
	for a in root:
		list = a.find('legos')
		if not list == None:
			#other datas
			# for every lego_data in legos
			for l in list:
				id = l.get('name')
				fileName = a.get('name')
				if not id == None:
					file.write(fileName+id+',')
					for d in l:
						tag = d.tag
						if tag not in excludeTag:
							if tag == 'BlockType':
								#s = str(typeNameDict[d.text])
								f = fileName[fileName.find('/')+1:]
								s = f[f.find('_')+1:f.rfind('_')]
								s = s[:s.find('_') if not s.find('_') == -1 else len(s)]
								s = str(typeNameDict[s])
							elif tag == 'BlockDimension':
								f = fileName[fileName.find('/')+1:]
								s = f[:f.find('_')].replace('x',',')
								s = '('+s+')'
							else:
								s = d.text
							
							s = s+','
							file.write(s)
					file.write('\n')

	file.close()