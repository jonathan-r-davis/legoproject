'''
Author: Jonathan Davis
Email: jonathanr.davis96@gmail.com
'''

import os
import cv2 as cv
import tensorflow as tf
import keras
from keras.utils import np_utils
import numpy as np
import matplotlib.pyplot as plt

curLossAvg = 0.21391350001932483
curAccAvg = 0.934990406036377

i = 0
losses = []
acc = []

while i < 3:
    train_images = []
    train_labels = []
    test_images = []
    test_labels = []

    #Get training images & labels
    file = open('./database/training'+ str(i) + '/log.csv','r')
    while True:
        line = file.readline().split(',')
        if len(line) == 1:
            break
        imageName = line[0]+".png"
        image = cv.imread(imageName) #color
        # image = cv.imread(imageName, 0) #b/w
        train_labels.append(line[1])
        train_images.append(image)
    train_labels = np.array(train_labels)
    train_images = np.array(train_images)
    file.close()

    #Get testing images & labels
    file = open('./database/testing' + str(i) + '/log.csv','r')
    while True:
        line = file.readline().split(',')
        if len(line) == 1:
            break
        imageName = line[0]+".png"
        image = cv.imread(imageName) #color
        # image = cv.imread(imageName, 0) #b/w
        test_labels.append(line[1])
        test_images.append(image)
    test_labels = np.array(test_labels)
    test_images = np.array(test_images)
    file.close()

    # Process training & testing images and labels the same way
    train_images = train_images.astype('float32') / 255
    train_images = train_images.reshape(train_images.shape[0], 64, 64, 3) #color
    # train_images = train_images.reshape(train_images.shape[0], 64, 64, 1) #b/w
    train_labels = np_utils.to_categorical(train_labels, 7)

    test_images = test_images.astype('float32') / 255
    test_images = test_images.reshape(test_images.shape[0], 64, 64, 3) #color
    # test_images = test_images.reshape(test_images.shape[0], 64, 64, 1) #b/w
    test_labels = np_utils.to_categorical(test_labels, 7)


    #Build Model , this is working one
    #@ 30 epochs..
    # Losses: (0.27636380200176347, 0.17038050460598891, 0.19499619345022204). Average: 0.21391350001932483
    # Accuracy: (0.9311663508415222, 0.9445506930351257, 0.9292542934417725). Average: 0.934990406036377
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(32, (3,3), activation='relu', input_shape=(64,64,3)))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3,3), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Conv2D(128, (3,3), activation='relu'))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(1000, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.25))
    model.add(tf.keras.layers.Dense(7, activation='softmax'))

    #Compile model
    model.compile(loss='categorical_crossentropy',
                optimizer='adam',
                metrics=['accuracy'])

    #Fit model
    # model.fit(train_images, train_labels, epochs=20)
    model.fit(train_images,train_labels,
            batch_size=50,epochs=22, verbose=1, validation_data=(test_images, test_labels))

    # model.save('./model' + str(i))

    # Evaluate the model on test set
    test_loss, test_acc = model.evaluate(test_images, test_labels)

    losses.append(test_loss)
    acc.append(test_acc)

    i += 1

lossAvg = (losses[0] + losses[1] + losses[2]) / 3
print(f"\nLosses: ({losses[0]}, {losses[1]}, {losses[2]}). Average: {lossAvg}")

accAvg = (acc[0] + acc[1] + acc[2]) / 3
print(f"Accuracy: ({acc[0]}, {acc[1]}, {acc[2]}). Average: {accAvg}")

if lossAvg < curLossAvg and accAvg > curAccAvg:
    print("Better loss and accuracy!")
elif lossAvg < curLossAvg and accAvg < curAccAvg:
    print("Only loss is better.")
elif lossAvg > curLossAvg and accAvg > curAccAvg:
    print("Only accuracy is better.")
else:
    print("This has worse loss and accuracy..")
    